## ohos_coap单元测试用例

该测试用例基于OpenHarmony系统下，全量进行单元测试

### 单元测试用例覆盖情况

|接口名 | 是否通过 |备注|
|---|---|---|
|request|pass|
|setFormat|pass|
|setPort|pass|
|setWaitSecond|pass|
|setObsSecond|pass|
|setRepeatCount|pass|
|setPayload|pass|
|setToken|pass|
|setNativeLogOpen|pass|
|addOption|pass|
|setMid|pass|
|setPayloadBinary|pass|