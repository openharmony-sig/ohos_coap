/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 export interface AsyncCallback<T> {
  (data: T): void
}

declare class newCoapClient {
  classId: number;
  /**
   * 客户端发起coap请求
   */
  request(classId: number, coapUri: string, method: CoapRequestMethod, type: CoapRequestType): Promise<CoapResponse>;

  request(classId: number, coapUri: string, method: CoapRequestMethod, type: CoapRequestType, callback: AsyncCallback<CoapResponse>): void;
  
  /**
   * 客户端设置透传格式
   */
  setFormat: (format: ContentFormat, classId: number) => void;
  /**
   * 客户端使用put,post时透传参数
   */
  setPayload: (payload: string, classId: number) => void;

  /**
   * 客户端设置初始令牌
   */
  setToken: (token: Uint8Array, classId: number) => void;
  /**
   * 客户端设置请求端口号
   */
  setPort: (port: string, classId: number) => void;
  /**
   * 客户端设置请求等待超时时间
   */
  setWaitSecond: (waitSecond: number, classId: number) => void;

  /**
   * 客户端设置观察持续时间
   */
  setObsSecond: (obsSecond: number, classId: number) => void;

  /**
   * 客户端设置观察持续时间
   */
  setRepeatCount: (repeatCount: number, classId: number) => void;

  /**
   * 客户端设置option
   */
  addOption: (optionNum: number, value: string | number, classId: number) => void;

  /**
   * 客户端设置message id
   */
  setMid: (mid: number, classId: number) => void;

  /**
   * 客户端使用put,post时透传参数
   */
  setPayloadBinary: (payload: Uint8Array, classId: number) => void;

  /**
   * 注册自定义coap_option
   */
  registerOption: (option: number, classId: number) => void;

  /**
   * 设置blockMode
   */
  setBlockMode: (mode: number, classId: number) => void;
}

declare class newCoapServer {
  classId: number;

  /**
   * 启动服务器
   */
  start: (classId: number) => void;

  /**
   * 关闭服务器
   */
  stop: (classId: number) => void;

  /**
   * 添加资源
   */
  addResource: (classId: number,resourceID: string) => void;

  /**
   *设置服务IP和端口
   */
  setAddr: (classId: number,ip: string, port: string) => void;

  /**
   * 判断是否启动状态码
   */
  getStatus: (classId: number) => number
}

declare class newCoapResource {
  resourceID: string | number;

  /**
   * 添加get请求处理器
   */
  registerGetHandler: (resourceID: string, cb: Function) => void;

  /**
   * 添加post请求处理器
   */
  registerPostHandler: (resourceID: string, cb: Function) => void;

  /**
   * 添加资源属性
   */
  addAttributes: (resourceId: string, attributesOptionOne: string, attributesOptionTwo: string) => void;

  /**
   * 添加Id
   */
  createResource: (resourceId: string) => string;

  /**
   * 注销资源
   */
  destroy: () => void;
}

declare class newCoapExchange {

  /**
   *发送响应到客户端
   */
  response: (responseId: string, code: number, data: string) => void;

  /**
   *获取请求选项
   */
  getRequestOptions: (responseId: string) => string;

  /**
   *获取请求负载
   */
  getRequestText: (responseId: string) => string;

  /**
   *获取发起请求的对端IP
   */
  getSourceAddress: (responseId: string) => string;
}

/**
 * 打开native层的log,默认是关闭的
 */
export const setNativeLogOpen: (isOpen: boolean) => void;

export enum CoapRequestMethod {
  GET = 1,
  POST = 2,
  PUT = 3,
}

export enum CoapRequestType {
  COAP_MESSAGE_CON = 0,
  COAP_MESSAGE_NON = 1
}

export enum ContentFormat {
  PLAIN = 0, // text/plain (plain)
  LINK = 40, // application/link-format (link, link-format)
  XML = 41, // application/xml (xml)
  BINARY = 42, // application/octet-stream (binary, octet-stream)
  EXI = 47, // application/exi (exi)
  JSON = 50, // application/json (json)
  CBOR = 60 //   application/cbor (cbor)
}

export class CoapResponse {
  code: CoapResponseCode;
  message: string[];
}

/**
 * coap native回调状态码
 */
export enum CoapResponseCode {
  SUCCESS = 0, // request成功
  OTHER_ERROR = 1, // 其他异常
  NETWORK_ERROR = 2, // 网络错误
  URL_ERROR = 3 // url错误,不是正确的coap url
}