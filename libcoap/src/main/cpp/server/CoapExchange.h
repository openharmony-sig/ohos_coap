/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBCOAP_EXCHANGE_H
#define LIBCOAP_EXCHANGE_H
#include <semaphore.h>

#include <string>

#include "base.h"

class CoapExchange {
public:
    using ResponseTool = struct ResponseTool {
        coap_resource_t *resource;
        coap_session_t *session;
        const coap_pdu_t *request;
        const coap_string_t *query;
        coap_pdu_t *response;
        sem_t sem;
    };

    static void Destroy(std::string responseId);
    static ResponseTool SetResponseTool(coap_resource_t *resource, coap_session_t *session, const coap_pdu_t *request,
                                        const coap_string_t *query, coap_pdu_t *response);
    static void Wait(std::string responseId);
    static void AddResponse(std::string responseId, CoapExchange::ResponseTool responseTool);
    static napi_value Response(napi_env env, napi_callback_info info);
    static napi_value GetRequestOptions(napi_env env, napi_callback_info info);
    static napi_value GetRequestText(napi_env env, napi_callback_info info);
    static napi_value GetSourceAddress(napi_env env, napi_callback_info info);
    static napi_value JsConstructor(napi_env env, napi_callback_info info);
};

#endif  // LIBCOAP_EXCHANGE_H