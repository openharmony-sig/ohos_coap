/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBCOAP_SERVER_H
#define LIBCOAP_SERVER_H
#include <string>

#include "base.h"
#include "CoapResource.h"

const int DEFAULT_BUFF_SIZE = 1024;
const int ONE_SECOND = 1000;

const int STOP = 0;
const int RUNNING = 1;
const int WAITSTOP = 2;

class Server {
public:
    Server();
    static napi_value Start(napi_env env, napi_callback_info info);
    static napi_value Stop(napi_env env, napi_callback_info info);
    static napi_value JsConstructor(napi_env env, napi_callback_info info);
    static napi_value AddResource(napi_env env, napi_callback_info info);
    static napi_value SetAddr(napi_env env, napi_callback_info info);
    static napi_value GetStatus(napi_env env, napi_callback_info info);
    unsigned int GetRunning();
    void SetRunning(unsigned int status);

    std::string classIdStr;
    std::vector<coap_resource_t *> resources;
    char ip[DEFAULT_BUFF_SIZE];
    char port[DEFAULT_BUFF_SIZE];
private:
    unsigned int running;
};  // namespace OhosLogServer

#endif  // LIBCOAP_SERVER_H