/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBCOAP_RESOURCE_H
#define LIBCOAP_RESOURCE_H

#include <string>

#include "base.h"

class CoapResource {
public:
    using ResourceHandler = struct ResourceHandler {
        std::string uri;
        coap_resource_t *resource;
        napi_threadsafe_function getHandler;
        napi_threadsafe_function postHandler;
    };

    CoapResource() {}
    ~CoapResource() {}
    static bool ParamFilter(napi_env env, napi_callback_info info,
                                     napi_threadsafe_function *tsfn,
                                     coap_resource_t **resource);
    static napi_value Destroy(napi_env env, napi_callback_info info);
    static napi_threadsafe_function getPostHandler(std::string resourceId);
    static napi_threadsafe_function getGetHandler(std::string resourceId);
    static std::string setResourceHandler(coap_resource_t *resource, std::string uri,
                                          napi_threadsafe_function getHandler = nullptr,
                                          napi_threadsafe_function postHandler = nullptr);

    static napi_value RegisterPostHandler(napi_env env, napi_callback_info info);
    static napi_value RegisterGetHandler(napi_env env, napi_callback_info info);
    static napi_value AddAttributes(napi_env env, napi_callback_info info);
    static napi_value JsConstructor(napi_env env, napi_callback_info info);
    static napi_value CreateResource(napi_env env, napi_callback_info info);
    static coap_resource_t *getResource(std::string resourceId);
};

#endif  // LIBCOAP_RESOURCE_H