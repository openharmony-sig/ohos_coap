#!/bin/sh
#
# Copyright (c) 2024 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

cp ./0001-CVE-2024-0962.patch ./0001-CVE-2024-31031.patch ./0001-CVE-2024-46304.patch ../libcoapSource/
cd ../libcoapSource/
patch -p1 < 0001-CVE-2024-0962.patch
patch -p1 < 0001-CVE-2024-31031.patch
patch -p1 < 0001-CVE-2024-46304.patch
rm -rf 0001-CVE-2024-0962.patch 0001-CVE-2024-31031.patch 0001-CVE-2024-46304.patch