/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ohos_log.h"
#include "third_party_bounds_checking_function/include/securec.h"
#include <cstdio>
#include <hilog/log.h>

const int OHOS_LOG_BUF_SIZE = 4096;
bool g_ohosLogOn = false; // log开关、默认关
namespace OhosLog {
napi_value SetLogOpen(napi_env env, napi_callback_info info)
{
    size_t argc = 1;
    napi_value args[1] = {nullptr};
    napi_get_cb_info(env, info, &argc, args, nullptr, nullptr);
    napi_get_value_bool(env, args[0], &g_ohosLogOn);
    OH_LOG_Print(LOG_APP, LOG_DEBUG, 0, "libcoap", "SetLogOpen:%{public}d", g_ohosLogOn);
    napi_value result = nullptr;
    napi_get_undefined(env, &result);
    return result;
}
} // namespace OhosLog
void OhosLogPrint(CoapLogLevel level, const char *tag, const char *fmt, ...)
{
    if (!g_ohosLogOn) {
        return;
    }
    char buf[OHOS_LOG_BUF_SIZE] = { 0 };
    int nSize = 0;
    int len = sizeof(buf);
    va_list arg;
    va_start(arg, fmt);
    nSize = vsnprintf_s(buf, len, OHOS_LOG_BUF_SIZE, fmt, arg);
    if (nSize < 0) {
        return;
    }
    switch (level) {
        case CoapLogLevel::IL_INFO:
            OH_LOG_Print(LOG_APP, LOG_INFO, LOG_DOMAIN, tag, "%{public}s", buf);
            break;
        case CoapLogLevel::IL_DEBUG:
            OH_LOG_Print(LOG_APP, LOG_DEBUG, LOG_DOMAIN, tag, "%{public}s", buf);
            break;
        case CoapLogLevel::IL_WARN:
            OH_LOG_Print(LOG_APP, LOG_WARN, LOG_DOMAIN, tag, "%{public}s", buf);
            break;
        case CoapLogLevel::IL_ERROR:
            OH_LOG_Print(LOG_APP, LOG_ERROR, LOG_DOMAIN, tag, "%{public}s", buf);
            break;
        case CoapLogLevel::IL_FATAL:
            OH_LOG_Print(LOG_APP, LOG_FATAL, LOG_DOMAIN, tag, "%{public}s", buf);
            break;
        default:
            OH_LOG_Print(LOG_APP, LOG_INFO, LOG_DOMAIN, tag, "%{public}s", buf);
            break;
    }
    va_end(arg);
}