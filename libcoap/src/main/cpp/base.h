/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBCOAP_BASE_H
#define LIBCOAP_BASE_H
#include <arpa/inet.h>
#include <netdb.h>
#include <hilog/log.h>
#include "napi/native_api.h"
#include "log/ohos_log.h"
#ifdef __cplusplus
extern "C"
{
#endif

#include "coap3/libcoap.h"
#include "coap3/coap_forward_decls.h"
#include "coap3/coap_block.h"
#include "coap3/coap_address.h"
#include "coap3/coap_async.h"
#include "coap3/coap_cache.h"
#include "coap3/coap_dtls.h"
#include "coap3/coap_event.h"
#include "coap3/coap_io.h"
#include "coap3/coap_prng.h"
#include "coap3/coap_option.h"
#include "coap3/coap_subscribe.h"
#include "coap3/coap_time.h"
#include "coap3/coap_encode.h"
#include "coap3/coap_mem.h"
#include "coap3/coap_net.h"
#include "coap3/coap_pdu.h"
#include "coap3/coap_resource.h"
#include "coap3/coap_str.h"
#include "coap3/coap_uri.h"

#ifdef __cplusplus
}
#endif


inline int Min(int a, int b)
{
    return  a < b ? a : b;
}
#endif // LIBCOAP_BASE_H
