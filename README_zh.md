# ohos_coap

## 介绍

libcoap是Coap协议的C语言实现，它是使用Coap协议的工具。
ohos_coap是基于libcoap v4.3.1版本，封装napi接口，给上层ts提供coap通信能力的三方库。

目前客户端实现的能力：

- 支持coap客户端发起get,post,put请求

目前服务端实现的能力：

- 支持coap服务端处理get，post数据

暂不支持的能力：

- coap服务端除注册get、post请求外其余功能不支持
- coap客户端发送文件，媒体软件能力
- 不支持tls/dtls的消息
- 不支持发起 delete请求


## 使用本工程

有两种方式可以下载本工程：

1. 开发者如果想要使用本工程,可以使用git命令

```
git clone https://gitee.com/openharmony-tpc/ohos_coap.git --recurse-submodules
```

2. 点击下载按钮，把本工程下到本地，再把libcoap v4.3.1版本代码放入libcoap/src/cpp/libcoapSource目录下，这样才可以编译通过。

3. 漏洞修复，进入到libcoap\src\main\cpp\thirdModule 目录下，执行 modify.sh 脚本，将本目录下的 patch 文件合入到 libcoap 源码中。
   详情查看[漏洞修复记录](https://gitee.com/openharmony-tpc/ohos_coap/blob/master/Vulnerability_Repair.md)

## 下载安装

```
ohpm install @ohos/coap
```

OpenHarmony ohpm
环境配置等更多内容，请参考[如何安装 OpenHarmony ohpm 包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md)


## X86模拟器配置

[使用模拟器运行应用/服务](https://developer.huawei.com/consumer/cn/deveco-developer-suite/enabling/kit?currentPage=1&pageSize=100)


## 使用

### 1、coap get 请求展示

```
// 打开native log开关
CoapClient.setNativeLogOpen(true);
// 发起coap get请求，注意this.coapUri请使用者换成真实的url
// 每一个CoapClient对应一个COAP请求任务，不可复用
let coapClient = new CoapClient();
let coapGet = coapClient.request(this.coapUri, CoapRequestMethod.GET, CoapRequestType.COAP_MESSAGE_CON);
console.log("libcoap get test");
//coap get请求异步回调
coapGet.then((data) => {
  if (data.code == CoapResponseCode.SUCCESS) { //sucess说明请求成功 
    console.log("libcoap get:" + data.message[0]);
  } else {
    console.log("libcoap get code:" + data.code);
    console.log("libcoap get error message:" + data.message);
  }
})

let coapClient = new CoapClient();
get请求CaLLBACK回调
coapClient.requestCall(this.coapUri, CoapRequestMethod.GET, CoapRequestType.COAP_MESSAGE_CON, (data: CoapResponse) => {
  if (data.code == CoapResponseCode.SUCCESS) {
    console.log("libcoap requestCall get:" + data.message[0]);
  } else {
    console.log("libcoap requestCall get code:" + data.code);
    console.log("libcoap requestCall get error message:" + data.message[0]);
  }
});
```

### 2、coap put 请求展示

```
// 打开native log开关
CoapClient.setNativeLogOpen(true);
let coapClient = new CoapClient();
//设置端口号
coapClient.setPort(5683);
//设置透传数据的格式
coapClient.setFormat(ContentFormat.PLAIN);
//设置透传数据
coapClient.setPayload("this is put test payload");
// 发起coap put请求，注意this.coapUri请使用者换成真实的url
// 每一个CoapClient对应一个COAP请求任务，不可复用
let coapPut = coapClient.request(this.coapUri, CoapRequestMethod.PUT, CoapRequestType.COAP_MESSAGE_CON);
console.log("libcoap put test");
//coap get请求异步回调
coapPut.then((data) => {
  if (data.code == CoapResponseCode.SUCCESS) { //sucess说明请求成功 
    console.log("libcoap put:" + data.message[0]);
  } else {
    console.log("libcoap put code:" + data.code);
    console.log("libcoap put error message:" + data.message);
  }
})
```

## 服务端使用

### 服务器注册get，post请求

```
let server = new CoapServer();
let resource = new CoapResource();
let exchange = new CoapExchange();
//服务器本地IP，端口号目前固定
server.setAddr('125.0.10.5', '0');
// 打开native log开关
CoapClient.setNativeLogOpen(true);
//添加id
let resourceId = this.resource.createResource('test');
//添加资源属性
resource.addAttributes(resourceId, 'title', 'Dynamic');
//处理get 数据
resource.registerGetHandler(resourceId, (responseId: string) => {
 let getQuesOptions = exchange.getRequestOptions(responseId);
 exchange.response(responseId, CoapResourceCode.COAP_RESPONSE_CODE_CONTENT, 'hello arkts');
});
//处理post 数据
resource.registerPostHandler(resourceId, (responseId: string) => {
let postQuesOptions = exchange.getRequestOptions(responseId);
let postQuesText = exchange.getRequestText(responseId); 
 exchange.response(responseId, CoapResourceCode.COAP_RESPONSE_CODE_CONTENT, 'hello arkts');
});
//添加资源
server.addResource(resourceId);
//coap server 启动服务器
server.start();
```

## 客户端接口说明

| 方法名                                                                                                                   | 入参                                                                 | 接口描述                    |
|-----------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------|-------------------------|
| request(coapUri: string, method: CoapRequestMethod, type: CoapRequestType)                                            | coapUri: string, method: CoapRequestMethod, type: CoapRequestTyp   | 客户端发起请求                 |
| requestCall(coapUri: string, method: CoapRequestMethod, type: CoapRequestType, callback: AsyncCallback<CoapResponse>) | coapUri: string, method: CoapRequestMethod, type: CoapRequestTyp, callback: AsyncCallback<CoapResponse> | 客户端发起请求                 |
| setFormat(format: ContentFormat)                                                                                      | format: ContentFormat                                              | 客服端发起post,put请求时透传参数的格式 |
| setPort(port: number)                                                                                                 | port: number                                                       | 设置端口号                   |
| setWaitSecond(waitSecond: number)                                                                                     | waitSecond: number                                                 | 客户端设置请求等待超时时间           |
| setObsSecond(obsSecond: number)                                                                                       | obsSecond: number                                                  | 客户端设置连接持续观察等待时间         |
| setRepeatCount(repeatCount: number)                                                                                   | repeatCount: number                                                | 客户端设置重试请求次数             |
| setPayload(payload: string)                                                                                           | payload: string                                                    | 设置客服端透传的内容              |
| setToken(token: Uint8Array)                                                                                           | token: Uint8Array                                                  | 设置token                 |
| setNativeLogOpen(isOpen: boolean)                                                                                     | isOpen: boolean                                                    | 是否打开native侧的log         |
| addOption(optionNum: number, value: string)                                                                           | optionNum: number, value: string                                   | 客户端自定义新增一个option选项       |
| setMid(mid: number)                                          	                                                        | mid: number                                                        | 客户端自定义报文的message id         |
| setPayloadBinary(payload: Uint8Array)                                                                                 | payload: Uint8Array                                                | 客户端透传的字节内容   
| setBlockMode(mode: CoapBlockMode)                                                                                     | mode: CoapBlockMode                                                | 客户端的块传输模式   

单元测试用例详情见[TEST.md](https://gitee.com/openharmony-tpc/ohos_coap/blob/master/TEST.md)

## server端接口说明

| 方法名                                                                                           | 入参                                                                             | 接口描述               |
|-----------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------|--------------------|
| start(classId: number)                                                                        | classId: number                                                                | 启动服务器              |
| stop(classId: number)                                                                         | classId: number                                                                | 关闭服务器              |
| addResource(classId: number, resourceID: string)                                              | resourceID: string                                                             | 添加资源               |
| setAddr(classId: number, ip: string, port: string)                                            | ip: string, port: string                                                       | 设置服务IP和端口(0表示默认端口) |
| getStatus(classId: number)                                                                    | classId: number                                                                | 判断是否启动状态码          |
| registerGetHandler(resourceID: string, cb: Function)                                          | resourceID: string, cb: Function                                               | 添加get请求处理器         |
| registerPostHandler(resourceID: string, cb: Function)                                         | resourceID: string, cb: Function                                               | 添加post请求处理器        |
| addAttributes: (resourceId: string, attributesOptionOne: string, attributesOptionTwo: string) | (resourceId: string, attributesOptionOne: string, attributesOptionTwo: string) | 添加资源属性             |
| createResource: (resourceId: string)                                                          | resourceId: string                                                             | 添加Id               |
| destroy()                                                                                     |                                                                                | 注销资源               |
| response: (responseId: string, code: number, data: string)                                    | (responseId: string, code: number, data: string)                               | 发送响应到客户端           |
| getRequestOptions: (responseId: string)                                                       | responseId: string                                                             | 获取请求选项             
| getRequestText: (responseId: string)                                      	                | responseId: string                                                             | 获取请求负载             |
| getSourceAddress: (responseId: string)                                                        | responseId: string                                                             | 对端IP            |

## 约束与限制

在下述版本验证通过：

- DevEco Studio: NEXT Developer Beta1-5.0.3.331, SDK: API12(5.0.0.25(SP6))
- 在DevEco Studio: NEXT Beta1-5.0.3.806, SDK: API12 Release (5.0.0.66)上验证通过

## 目录结构

```
|----ohos_coap
|     |---- entry  # 示例代码文件夹
|     |---- libcoap  # ohos_coap库文件夹
|           |---- index.ets  # 对外接口
|     |---- README_CN.md  # 使用说明文档
```

## 贡献代码

使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-tpc/ohos_coap/issues)
，当然，也非常欢迎发 [PR](https://gitee.com/openharmony-tpc/ohos_coap/pulls) 共建。

## 开源协议

本项目基于 [Apache License 2.0 ](https://gitee.com/openharmony-tpc/ohos_coap/blob/master/LICENSE) ，请自由地享受和参与开源。
依赖libcoap基于 [BSD 2](https://gitee.com/openharmony-tpc/ohos_coap/blob/master/LICENSE-BSD2-libcoap) ，请自由地享受和参与开源。