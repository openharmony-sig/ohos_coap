## 2.0.12

- Fixed the issue of a discrepancy between the set timeout and the actual timeout in multicast scenarios
- Roll back libcoap version to 4.3.4
- Add patch to fix vulnerability CVE-2024-46304
- Fix asan alarms and errors

## 2.0.11

- Upgrade the source library version to v4.3.5
- Fix the memory leak issue of OhosLogPrint function

## 2.0.10

- 在DevEco Studio: NEXT Beta1-5.0.3.806, SDK: API12 Release (5.0.0.66)上验证通过

## 2.0.9

- 修复客户端子网地址广播失败问题：开启HAVE_IFADDRS_H宏

## 2.0.8

- 新增setBlockMode接口：设置客户端的块传输模式
- start接口返回值修改为void

## 2.0.8-rc.0

- 支持coap服务端处理get，post请求

## 2.0.7

- 新增requestCall接口
- SetWaitSecond接口优化：入参接受浮点数

## 2.0.7-rc.0

- 新增requestCall接口
- SetWaitSecond接口优化：入参接受浮点数

## 2.0.6

- 修复依赖库libcoap漏洞：CVE-2024-0962、CVE-2024-31031

## 2.0.5

- 支持x86编译

## 2.0.5-rc.0

- 解决RegisterOption注册和AddOption支持三种类型
- 修改libcoap许可证信息和Readme说明文档
- 修改C++部分函数命名风格

## 2.0.4

- 客户端自定义新增一个option选项
- 客户端定义报文的message id
- 客户端发送字节形式的payload
- 通过Get方式收到返回消息时，携带对端Ip
- token长度限制由固定的8改为协议标准的3~8

## 2.0.4-rc.0

- libcoap功能增强：
- 客户端自定义新增一个option选项
- 客户端定义报文的message id
- 客户端发送字节形式的payload
- 通过Get方式收到返回消息时，携带对端Ip
- token长度限制由固定的8改为协议标准的3~8

## 2.0.3

- 修复漏洞CVE-2023-30362：libcoap版本由4.3.1切换至4.3.4。

## 2.0.2

- ArkTs新语法适配
- 解决发送多次请求，回调结果相互影响的问题

## 2.0.1

- 增加编译选项
- 适配DevEco Studio: 4.0 Release (4.0.3.415)
- 适配SDK: API10 (4.0.10.5)

## 2.0.0

- 删除coap头文件直接引用,使用cmakeList configure_file生成头文件
- 增加英文readme
- 修改hilog的头文件引用
- 适配DevEco Studio:  4.0 Canary1(4.0.3.212)
- 适配SDK: API 10 Release(4.0.8.3)

## 1.0.0

libcoap是Coap协议的C语言实现，它是使用Coap协议的工具。
ohos_coap基于libcoap v4.3.1版本，封装napi接口，给上层ts提供coap通信能力。

目前实现的能力：
- 支持coap客户端发起get,post,put请求

暂不支持的能力：

- coap服务端相关能力
- coap客户端发送文件，媒体软件能力
- 不支持tls/dtls的消息
- 不支持发起 delete请求
